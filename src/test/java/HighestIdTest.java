import Utils.IdUtils;
import org.testng.Assert;
import org.testng.annotations.Test;

public class HighestIdTest extends BaseTest {

    @Test
    public void highestId(){
        Assert.assertEquals( IdUtils.getHighestIdForUserId(IdUtils.getHighestUserIdValue()), 100, "Highest userId value doesn't match" );
    }
}
