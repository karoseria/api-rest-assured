import io.restassured.response.Response;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.annotations.Test;

import static Utils.IdUtils.getHighestIdForUserId;
import static Utils.IdUtils.getHighestUserIdValue;
import static io.restassured.RestAssured.when;
import static io.restassured.http.ContentType.JSON;
import static io.restassured.RestAssured.given;

public class AddCommentTest extends BaseTest {

    @Test
    public void addCommentToGivenPost(){
        Integer highestPostId = getHighestIdForUserId(getHighestUserIdValue());
        JSONObject jsonObj = new JSONObject()
                .put("postId", highestPostId)
                .put("name","test_comment")
                .put("email","test@email")
                .put("body", "some test message");

        Response response=
                given()
                        .contentType("application/json")
                        .body(jsonObj.toString()).
                when()
                        .post((highestPostId.toString() +"/comments")).
                then()
                        .contentType(JSON)
                        .extract()
                        .response();

        Assert.assertEquals(response.statusCode(), 201);
        Assert.assertEquals(response.jsonPath().getInt("id"), 501);
    }


}
