import org.testng.Assert;
import org.testng.annotations.Test;

import static Utils.IdUtils.getHighestUserIdValue;

public class HighestUserIdTest extends BaseTest{

    @Test
    public void highestUserIdIs10(){
        Assert.assertEquals( getHighestUserIdValue(), 10, "Highest userId value doesn't match" );
    }
}
