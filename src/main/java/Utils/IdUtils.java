package Utils;

import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;

import static io.restassured.RestAssured.given;
import static io.restassured.RestAssured.when;
import static io.restassured.http.ContentType.JSON;

public class IdUtils {

    public static int getHighestUserIdValue(){
        Response response =
                when().
                        get("/").
                then().
                        contentType(JSON).
                        extract().
                        response();

        return new JsonPath(response.asString()).getInt("userId.max()");
    }

    public static int getHighestIdForUserId(Integer userId){
        Response response =
                given()
                        .param("userId", userId.toString()).
                when()
                        .get("/").
                then()
                        .contentType(JSON)
                        .extract()
                        .response();

        return new JsonPath(response.asString()).getInt("id.max()");
    }
}
